using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net.Sockets;
using System.Text;
using TMPro;
using System.Threading;

public class testscript : MonoBehaviour
{
    private string serverIP = "192.168.127.128";
    private int port = 9000;
    private NetworkStream stream;
    private TcpClient tcpClient;
    private Coroutine readCoroutine;
    private string IdName;
    private string chadTextData;
    private Thread readThread;
    private string ReadMessage;
    private int bytes;
    [SerializeField]
    private GameObject LoginBG;
    [SerializeField]
    private TMP_InputField sendmessagefield;

    [SerializeField]
    private TMP_InputField idfield;
    [SerializeField]
    private TMP_InputField passwordField;

    [SerializeField]
    private GameObject MakeIdPopUpUI;
    [SerializeField]
    private TMP_InputField newidfield;
    [SerializeField]
    private TMP_InputField newpasswordField;

    [SerializeField]
    private GameObject LoginFaildUI;

    [SerializeField]
    private GameObject MakeIDBG;

    [SerializeField]
    private TextMeshProUGUI ClientsListText;
    [SerializeField]
    private TextMeshProUGUI chatText;

    [SerializeField]
    private GameObject DuplicateIdPopUp;
    private void Start()
    {
        ConnectServer();
    }
    public void ConnectServer()
    {
        tcpClient = new TcpClient(serverIP, port);
        if (tcpClient.Connected)
        {
            stream = tcpClient.GetStream();
            readThread = new Thread(ReadDataThread);
            readThread.Start();
        }
    }
    private void OnApplicationQuit()
    {
        ExitServer();
    }
    private void Update()
    {
        if(bytes>0)
        {
            CheckMessage(ReadMessage);
            bytes =0;
        }
    }
    private void CheckMessage(string msg)
    {
        if (msg == "There is no password" || msg == "There is no id")
        {
            idfield.text = "";
            passwordField.text = "";
            LoginFaildUI.SetActive(true);
        }
        else if (msg == "loginsucceed")
        {
            MakeIDBG.SetActive(false);
            LoginBG.SetActive(false);
        }
        else if (msg.Contains("ClientList", StringComparison.OrdinalIgnoreCase))
        {
            SetChatClients(msg);
        }
        else if(msg== "Duplicate")
        {
            DuplicateIdPopUp.SetActive(true);
        }
        else
        {
            SetChatText(msg);
        }
    }
    public void ExitButton()
    {
        CloseSocket();
        ConnectServer();
        chatText.text = "";
        chadTextData = "";
    }
    //id만들기 버튼 클릭시
    public void MakeNewID()
    {
        MakeIdPopUpUI.SetActive(true);
        string message = "MakeNewID";
        byte[] data = Encoding.ASCII.GetBytes(message);
        stream.Write(data, 0, data.Length);
    }
    private void SetChatText(string data)
    {
        chadTextData += data + "\n";
        chatText.text = chadTextData;
        idfield.text = "";
        passwordField.text = "";
    }
    //새로 만들때
    public void SendNewID_Password()
    {
        string id = newidfield.text;
        string password = newpasswordField.text;
        string senddata = id + "/" + password;
        IdName = id;
        byte[] data = Encoding.ASCII.GetBytes(senddata);
        stream.Write(data, 0, data.Length);
        MakeIdPopUpUI.SetActive(false);
        newidfield.text = "";
        newpasswordField.text = "";
    }
    //일반 로그인용
public void SendID_Password()
    {
        string id = idfield.text;
        IdName = id;
        string password = passwordField.text;
        string senddata = id + "/" + password;
        byte[] data = Encoding.ASCII.GetBytes(senddata);
        stream.Write(data, 0, data.Length);
        idfield.text = "";
        passwordField.text = "";
    }
    public void SendMessage()
    {
        string messgae = sendmessagefield.text;
        string senddata="["+ IdName + "] : " + messgae;
        byte[] data = Encoding.ASCII.GetBytes(senddata);
        stream.Write(data, 0, data.Length);
    }
    public void ExitServer()
    {
        string message = "exit";
        byte[] data = Encoding.ASCII.GetBytes(message);
        stream.Write(data, 0, data.Length);
    }
    public void CloseSocket()
    {
        ExitServer();
        readThread.Abort();
        LoginBG.SetActive(true);
    }
    public void SetChatClients(string list)
    {
        ClientsListText.text = "";
        string[] parts = list.Split("/");
        for(int i=1;i< parts.Length;i++)
        {
            if(parts[i]!=IdName)
                ClientsListText.text += parts[i] + "\n";
            else
                ClientsListText.text += parts[i]+"(Client)" + "\n";
        }
    }
    void ReadDataThread()
    {
        byte[] buffer = new byte[1024];
        while(true)
        {
            if (stream.DataAvailable)
            {
                bytes = stream.Read(buffer, 0, buffer.Length);
                if (bytes > 0)
                {
                    string response = Encoding.ASCII.GetString(buffer, 0, bytes);
                    ReadMessage = response;
                }
                else if (bytes == 0)
                {
                    Debug.Log("서버종료");
                    CloseSocket();
                    break;
                }
            }
        }
        
        //try
        //{

        //}
        //catch (Exception e)
        //{
        //    Debug.Log("서버 비정상종료");
        //    CloseSocket();
        //}
    }
}
